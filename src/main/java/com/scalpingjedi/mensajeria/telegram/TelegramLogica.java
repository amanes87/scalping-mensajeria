package com.scalpingjedi.mensajeria.telegram;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.scalpingjedi.mensajeria.comentario.Comentario;
import com.telegram.service.TelegramSv;

import lombok.NoArgsConstructor;

@NoArgsConstructor
@Component
public class TelegramLogica {
	
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	private TelegramSv telegramSv;
	
	public TelegramLogica (String chatId, String url) {
		this.telegramSv = new TelegramSv(chatId, url);
	}
	
	public void enviarNotificacion(Comentario comentario) {
		log.info("Enviando comentario por telegram: {}", comentario.getId());
		telegramSv.notifyMessage(comentario.getMensaje());
	}

}
