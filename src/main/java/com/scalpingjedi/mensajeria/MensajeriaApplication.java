package com.scalpingjedi.mensajeria;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.scalpingjedi.mensajeria.comentario.Comentario;
import com.scalpingjedi.mensajeria.comentario.ComentarioRepository;
import com.scalpingjedi.mensajeria.discord.DiscordLogica;
import com.scalpingjedi.mensajeria.email.EmailLogica;
import com.scalpingjedi.mensajeria.telegram.TelegramLogica;

@SpringBootApplication
@ComponentScan({ "com.scalpingjedi", "com.telegram" })
@EntityScan("com.scalpingjedi")
@EnableJpaRepositories("com.scalpingjedi")
public class MensajeriaApplication implements CommandLineRunner  {
	
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Value("${telegram.chatId}")
	private String chatId;
	
	@Value("${telegram.url}")
	private String url;
	
	@Autowired
	private EmailLogica logicaEmail;
	
	@Autowired
	private DiscordLogica logicaDiscord;
	
	@Autowired
	private ComentarioRepository comentarioSv;

	
	public static void main(String[] args) {
		SpringApplication.run(MensajeriaApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		log.info("=================== MENSAJERIA INICIO =================== ");
		List<Comentario> comentariosNoPusheados = comentarioSv.todosRegistrosNoPusheadosAscendiente();
		TelegramLogica telegramLogica = new TelegramLogica(chatId, url);
		comentariosNoPusheados.forEach(comentario -> {
			comentario.setPusheado(1);
			comentarioSv.save(comentario);
			telegramLogica.enviarNotificacion(comentario);
			logicaDiscord.enviarNotificacion(comentario);
		});
		
		/*
		List<Comentario> comentariosNoEmaileados = comentarioSv.todosRegistrosNoEmaileadosAscendiente();
		comentariosNoEmaileados.forEach(comentario -> {
			comentario.setEnviado(1);
			comentarioSv.save(comentario);
			logicaEmail.enviarNotificacion(comentario);
		});
		*/
		log.info("=================== MENSAJERIA FIN =================== ");
		
		
	}

	

}
