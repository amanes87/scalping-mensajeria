package com.scalpingjedi.mensajeria.comentario;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.util.StringUtils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "comentario")
public class Comentario {

	@Id
	private Integer id;
	
	private String fecha;
	
	private String comentario;
	
	private int enviado;
	
	private int pusheado;
	
	public String getMensaje() {
		String mensaje = "";
		if (!StringUtils.isEmpty(getComentario())) {
			mensaje = getComentario().replace("<br />", "");
		}
		return mensaje;
	}
}
