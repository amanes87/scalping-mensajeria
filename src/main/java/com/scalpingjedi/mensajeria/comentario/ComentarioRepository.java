package com.scalpingjedi.mensajeria.comentario;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ComentarioRepository extends CrudRepository<Comentario, Integer>{

	@Query(value = "SELECT c FROM Comentario c WHERE c.enviado = 0 order by c.fecha ")
	List<Comentario> todosRegistrosNoEmaileadosAscendiente();
	
	@Query(value = "SELECT c FROM Comentario c WHERE c.pusheado = 0 order by c.fecha ")
	List<Comentario> todosRegistrosNoPusheadosAscendiente();
	
}
