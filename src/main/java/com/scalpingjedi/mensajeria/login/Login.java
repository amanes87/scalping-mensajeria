package com.scalpingjedi.mensajeria.login;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "login")
public class Login {

	@Id
	private Integer id;
	private int activo;
	private String email;
	private String password;
	private int cookie;
}
