package com.scalpingjedi.mensajeria.login;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface LoginRepository extends CrudRepository<Login, Long> {

	@Query(nativeQuery = true, value = "SELECT email FROM login where activo = 1 order by id ")
	public List<String> todosRegistrosActivos();

}
