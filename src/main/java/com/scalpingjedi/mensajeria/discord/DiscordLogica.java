package com.scalpingjedi.mensajeria.discord;

import java.io.DataOutputStream;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.scalpingjedi.mensajeria.comentario.Comentario;

@Component
public class DiscordLogica {
	
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	@Value("${discord.url}")
	private String url;
	
	public void enviarNotificacion(Comentario comentario) {
		log.info("Enviando comentario por discord: {}", comentario.getId());
		try {
			sendPost(URLEncoder.encode(comentario.getMensaje(), "UTF-8"));
		} catch (Exception e) {
			log.error("Error durante el envío del mensaje de Discord {}, error: {}", comentario.getMensaje(), e);
		}
	}
	
	private void sendPost(String mensaje) throws Exception {

		String USER_AGENT = "Mozilla/5.0";
		
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		
		String urlParameters = "content=" + mensaje;
		
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		
		int responseCode = con.getResponseCode();
		log.info("Respuesta: {}", responseCode);

	}

}
