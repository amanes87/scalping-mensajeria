package com.scalpingjedi.mensajeria.email;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.scalpingjedi.mensajeria.comentario.Comentario;
import com.scalpingjedi.mensajeria.email.sendgrid.SendGridHelper;

@Component
public class EmailHelper {
	
	@Autowired
	private SendGridHelper sendGridHelper;
	
	private final String remitente = "soporte@scalpingjedi.com";
	
//	private final String sujeto = "Mensaje de Ivan: ¡Nueva Entrada de ScalpingJEDI!";
	private final String sujeto = "Scalping Jedi Entrada";
	
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	public void envioEmailPendiente(Comentario comentario, List<String> destinatarios) {
		destinatarios.stream().forEach(email -> {
			try {
				sendGridHelper.mandarEmail(remitente, email, sujeto, comentario.getMensaje());
			} catch (IOException e) {
				log.error("Excepcion durante el del email {}, error {}", email, e);
			}
		});
		
	}
	
	
}
