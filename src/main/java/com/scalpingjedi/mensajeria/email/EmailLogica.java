package com.scalpingjedi.mensajeria.email;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.scalpingjedi.mensajeria.comentario.Comentario;
import com.scalpingjedi.mensajeria.login.LoginRepository;

@Component
public class EmailLogica {
	
	@Autowired
	private LoginRepository loginSv;

	@Autowired
	private EmailHelper emailHelper;
	
	public void enviarNotificacion(Comentario comentario) {
//		List<String> destinatarios = loginSv.todosRegistrosActivos();
		List<String> destinatarios = Arrays.asList("adriamanes@gmail.com", "atomillerom@gmail.com");
		emailHelper.envioEmailPendiente(comentario, destinatarios);
	}

}
