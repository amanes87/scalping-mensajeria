package com.scalpingjedi.mensajeria.email.sendgrid;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;

@Component
public class SendGridHelper {
	
	@Value("${SEND_GRID_API_KEY}")
	private  String API_KEY;
	
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	public void mandarEmail(String remitente, String destinatario, String sujeto, String cuerpo) throws IOException {
		Email from = new Email(remitente);
	    Email to = new Email(destinatario);
	    
	    Content content = new Content("text/plain", cuerpo);
	    Mail mail = new Mail(from, sujeto, to, content);

	    SendGrid sg = new SendGrid(API_KEY);
	    Request request = new Request();
	    try {
	      request.setMethod(Method.POST);
	      request.setEndpoint("mail/send");
	      request.setBody(mail.build());
	      Response response = sg.api(request);
	      log.debug("Status code: {}", response.getStatusCode());
	      log.debug("Response body: {}", response.getBody());
	      log.debug("Response headers: {}", response.getHeaders());
	    } catch (IOException ex) {
	    	log.error("EXCEPCION EN EL ENVIO");
	    	log.error("Destinatario: {} | Cuerpo: {} | excepción: {}", destinatario, cuerpo, ex);
	      throw ex;
	    }
	}
}
